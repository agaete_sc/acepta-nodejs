"use strict";

const soap = require('soap');
const select = require('xml-crypto').xpath;
const SignedXml = require('xml-crypto').SignedXml;
const DOMParser = require('xmldom').DOMParser;
const Decimal = require('decimal.js-light');

const AceptaWSSecurityCert = require('./AceptaWSSecurityCert');

const ENV = {
    INTEGRACION: {
        ca4xml: 'http://173.255.196.32:5001/ca4xml?wsdl',
    },
    CERTIFICACION: {
        ca4xml: 'http://173.255.196.32:5001/ca4xml?wsdl',
    },
    PRODUCCION: {
        ca4xml: 'http://173.255.196.32:5004/ca4xml?wsdl',
    }
};

class Acepta {

    /**
     * Página de transición según dicta Transbank.
     * @param urlRedirection obtenido de detailOutput al hacer getTransactionResult
     * @param token_ws
     * @param gifUrl? la url del gif de Transbank
     * @returns {string}
     */

    constructor(props) {
        //this.publicKey = props.publicKey;
        //this.privateKey = props.privateKey;
        //this.aceptaKey = props.aceptaKey;
        this.env = props.env || Acepta.ENV.INTEGRACION;
        this.verbose = props.verbose || false;

        //this._wsSecurity = new AceptaWSSecurityCert(this.privateKey,
        //    this.publicKey, 'utf8', true);
    }

    _getClient(type) {
        if(type !== 'ca4xml') {
            throw new Error('Acepta::_getClient invalid type parameter. Must be "ca4xml"');
        }
        const transactionClientKey = '_transactionClient_' + type;
        if(!this[transactionClientKey]) {
            return new Promise((resolve, reject) => {
                let options = {
                    ignoredNamespaces: {
                        namespaces: [],
                        override: true
                    },
                    attributesKey: '$attrs'
                };
                soap.createClient(this.env[type], options, (err, client) => {
                    if(err) {
                        return reject(err);
                    }

                    this[transactionClientKey] = client;
                    resolve(this[transactionClientKey]);
                    /*this._wsSecurity.promise().then(() => {
                        client.setSecurity(this._wsSecurity);
                        this[transactionClientKey] = client;
                        resolve(this[transactionClientKey]);
                    });*/
                });
            });
        }
        return Promise.resolve(this[transactionClientKey]);
    }

    ca4xmlOperation(props) {
        this.verbose && console.log('ca4xmlOperation:parameters', props);
        if(!props) {
            return Promise.reject(new Error('props param missing'));
        }

        return new Promise((resolve, reject) => {
            this._getClient('ca4xml').then((client) => {
                client.ca4xml.ca4xml.ca4xml(props,
                    (err, result, raw, soapHeader) => {
                        console.log('soapHeader', soapHeader);
                        console.log('==== request =====');
                        console.log(client.lastRequest);
                        console.log('==================');
                        console.log('==== response ====');
                        console.log(raw);
                        console.log('==================');

                        if(err) {
                            this.verbose && console.log('ca4xmlOperation:error!', err);
                            return reject(err);
                        }
                        this.verbose && console.log('ca4xmlOperation:result:', result);
                        if(this._verifySignature(raw)) {
                            resolve(result);
                        } else {
                            this.verbose && console.log('ca4xmlOperation: result doesn\'t have a valid signature!');
                            reject(new Error('Invalid signature response'));
                        }
                });
            }).catch((err) => {
                this.verbose && console.log('Promise rejection: ', err);
                reject(new Error('Promise rejection: ' + err));
            });
        });
    }



    _verifySignature(xml) {
        return true;
        try {
            let doc = new DOMParser().parseFromString(xml);
            let signature = select(doc, "//*[local-name(.)='Signature' and namespace-uri(.)='http://www.w3.org/2000/09/xmldsig#']")[0];
            let sig = new SignedXml();
            //Hack to check non-standard transbank SignedInfo node
            sig.validateSignatureValue = function () {
                let signedInfo = select(doc, "//*[local-name(.)='SignedInfo']");
                if (signedInfo.length == 0) throw new Error("could not find SignedInfo element in the message");
                let signedInfoCanon = this.getCanonXml([this.canonicalizationAlgorithm], signedInfo[0]);
                signedInfoCanon = signedInfoCanon.toString().replace("xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"", "xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"");
                let signer = this.findSignatureAlgorithm(this.signatureAlgorithm);
                let res = signer.verifySignature(signedInfoCanon, this.signingKey, this.signatureValue);
                if (!res) this.validationErrors.push("invalid signature: the signature value " + this.signatureValue + " is incorrect");
                return res
            };
            let webpayKey = this.webpayKey;
            sig.keyInfoProvider = {
                getKeyInfo: function (key, prefix) {
                    prefix = prefix || '';
                    prefix = prefix ? prefix + ':' : prefix;
                    return "<" + prefix + "X509Data></" + prefix + "X509Data>";
                },
                getKey: function (keyInfo) {
                    return webpayKey
                }
            };
            sig.loadSignature(signature);
            let res = sig.checkSignature(xml);
            if (!res) {
                throw new Error(sig.validationErrors.join('; '));
            }
            return res;
        } catch (err) {
            return false;
        }
    }

}

Acepta.ENV = ENV;

module.exports = Acepta;
