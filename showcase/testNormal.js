"use strict";

const Acepta = require('../lib/Acepta');
const express = require('express');
const bodyParser = require('body-parser');

let transactions = {};
let transactionsByToken = {};
let app = express();
app.use(bodyParser.urlencoded({ extended: true }));

//const cert = require('./cert/normal');

/**
 * 1. Instanciamos la clase Acepta.
 *
 * @type {Acepta}
 */
let acepta = new Acepta({
    //publicKey: cert.publicKey,
    //privateKey: cert.privateKey,
    //aceptaKey: cert.aceptaKey,
    verbose: true,
    env: Acepta.ENV.INTEGRACION
});

var createBill =  function(amount, date, rutMandante) {
    let netoValue = Math.round(amount*100/119);
    let ivaValue = amount - netoValue;

    let dataBill = {
        "BOLETA": {
          "Encabezado": {
            "IdDoc": {
              "TipoDTE": "39",
              "Folio": "1",
              "FchEmis": date,
              "IndServicio": "3",
            },
            "Emisor": {
              "RUTEmisor": "76414521-6",
              "RznSoc": "SAFECARD ACCESS TECHNOLOGIES SPA",
              "GiroEmis": "SERVICIOS INTEGRALES DE SEGURIDAD",
              "Acteco": "726000",
              "DirOrigen": "AUGUSTA GERONA 1577 OF 605",
              "CmnaOrigen": "LAS CONDES",
              "CiudadOrigen": "SANTIAGO"
            },
            "Receptor": {
              "RUTRecep": "66666666-6",
              "RznSocRecep": "CLIENTE",
              "GiroRecep": "PERSONA NATURAL",
              "DirRecep": "",
              "CmnaRecep": "",
              "CiudadRecep": ""
            },
            "Totales": {
                "MntNeto": ""+ netoValue,
                "IVA": ""+ ivaValue,
                "MntTotal": ""+ amount
            }
          },
          "Detalle": [
            {
              /*"CdgItem": {
                "TpoCodigo":"INT1",
                "VlrCodigo":"777"
              },*/
              "RUTMandante": rutMandante,
              "NroLinDet": "1",
              "NmbItem": "SERVICIO PARKING",
              "QtyItem": "1",
              "PrcItem": ""+ amount,
              "MontoItem": ""+ amount
            }
          ]
        }
    };

    return acepta.ca4xmlOperation({
              docid: '',
              comando: '',
              parametros: '',
              datos: dataBill
    });
}

app.get('/createBill', (req, res) => {
    createBill(150, "2018-01-01", "76389939-K").then((result) => {
        return res.send('Bill result:<br/>-------------<br/>' + JSON.stringify(result));
    }).catch(function (err) {
        return res.send('Bill err:<br/>-------------<br/>' + err);
    });
});

app.get('/', (req, res) => {
    res.send(`
<!DOCTYPE html>
<html>
    <head>
        <title>Test acepta-nodejs</title>
    </head>
    <body>
        <h1>Test acepta-nodejs</h1>
        <form action="/createBill" method="get">
            <p><input type="submit" value="Test createBill"></p>
        </form>
    </body>
</html>`);
});

app.listen(3000, () => {
    console.log('Server OK')
});
