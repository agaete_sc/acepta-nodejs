
# acepta-nodejs

Módulo no oficial para integrar la API de Acepta para la generación de documentos tributarios en el SII.

# Transacción normal

La explicación detallada de los ejemplos de código está en /tests

1)  Instanciar

```js

const Acepta = require('./lib/Acepta');

let wp = new WebPay({
    commerceCode: youCommerceCode,
    publicKey: youPublicKey,
    privateKey: youPrivateKey,
    aceptaKey: youAceptaKey,
    env: WebPay.ENV.INTEGRACION
});
```
